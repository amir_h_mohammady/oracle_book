1- create oracle teable:
		CREATE TABLE book_list(id number(10) NOT NULL,book_Name varchar2(45));
		ALTER TABLE book_list ADD author varchar2(45);
		ALTER TABLE book_list ADD ISBN varchar2(45);

2- make Id column as auto increment column:
		ALTER TABLE book_list ADD (CONSTRAINT dept_pk01 PRIMARY KEY (ID));
		CREATE SEQUENCE dept_seq01 START WITH 1;
		CREATE TRIGGER dept_bir01 BEFORE INSERT ON book_list FOR EACH ROW
		BEGIN
		  SELECT dept_seq01.NEXTVAL
		  INTO   :new.id
		  FROM   dual;
		END;

3- compile project:
		ant webexploded
		
4- deploy:
		deploy .\build\oracle-book