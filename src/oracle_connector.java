/**
 * Created by Amir on 9/12/2017.
 */
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;



public class oracle_connector extends HttpServlet{

    //private String message;
    private Connection co01;
    private Statement st01;


    public void init() throws ServletException
    {
        // Do required initialization
        //message = "Hello Amir02";
        //make connection
    }

    public void doGet(HttpServletRequest req01,
                      HttpServletResponse response)
            throws ServletException, IOException
    {
        // Set response content type
        response.setContentType("text/html");

        // Actual logic goes here.
        PrintWriter out = response.getWriter();

        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            //jdbc:oracle:thin:@localhost:1521:orcl
            //jdbc:mysql://localhost:3306/Amirphonebook
            co01 = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "lib", "abcd1234");
            st01 = co01.createStatement();
        } catch (Exception e){
            System.out.println("Cannot Make a Connection!"+ e);
        }
        //make top of html
        out.println("<html>");
        out.println("<body>");
        out.println("<form action=\"BookServlet\" method=\"GET\">");
        out.println("book name: <input type=\"text\" name=\"book_name\">");
        out.println("author: <input type=\"text\" name=\"author\" />");
        out.println("ISBN: <input type=\"text\" name=\"ISBN\" />");
        out.println("<input type=\"submit\" value=\"Add\" name=\"add\" />");
        out.println("</form><form action=\"BookServlet\" method=\"GET\">");
        out.println("book Name: <input type=\"text\" name=\"book_name\">");
        out.println("author: <input type=\"text\" name=\"author\" />");
        out.println("ISBN: <input type=\"text\" name=\"ISBN\" />");
        out.println("<input type=\"submit\" value=\"Search\" name=\"search\"/>");
        out.println("</form>");

        String Add01 = req01.getParameter("add");
        String Search01 = req01.getParameter("search");
        String Remove01 = req01.getParameter("remove");
        String book01 = req01.getParameter("book_name");
        String author01 = req01.getParameter("author");
        String ISBN01 = req01.getParameter("ISBN");
        ResultSet re01 = null;
        boolean bool01 = false;

        if (Add01 != null){
            //add id , first , last , phone;
            try {
                st01.executeUpdate("INSERT INTO book_list (book_name, author, ISBN) values ('" +  book01 + "','" + author01 + "','" + ISBN01 + "')");
            } catch (Exception e){
                //System.out.println(e);
                System.out.println("Cannot Add to database!"+ e);
            }
        } else if (Search01 != null) {
            try{
                if (book01 != ""){
                    re01 = st01.executeQuery("select * from book_list where book_Name like '%" + book01 + "%'");
                    bool01 = true;
                } else if (author01 != ""){
                    re01 = st01.executeQuery("select * from book_list where author like '%" + author01 + "%'");
                    bool01 = true;
                }else if (ISBN01 != ""){
                    re01 = st01.executeQuery("select * from book_list where ISBN like '%" + ISBN01 + "%'");
                    bool01 = true;
                }
            } catch (Exception e){
                System.out.println("Cannot Search!"+ e);
            }

            //make result page;
            if (bool01){
                out.println("<table border=\"0\" width=\"100%\" cellspacing=\"2\" cellpadding=\"2\">");
                out.println("<tr bgcolor=\"#E0E0FF\">");
                out.println("<td width=\"300\"><small><b>book Name:</b></small></td>");
                out.println("<td width=\"300\"><small><b>author:</b></small></td>");
                out.println("<td width=\"300\"><small><b>ISBN:</b></small></td>");
                try{
                    while(re01.next()){
                        out.println("<tr bgcolor=\"#E0ffE0\">");
                        out.println("<td width=\"300\"><small><b>" + re01.getString("book_Name") +"</b></small></td>");
                        out.println("<td width=\"300\"><small><b>" + re01.getString("author") +"</b></small></td>");
                        out.println("<td width=\"300\"><small><b>" + re01.getString("ISBN") +"</b></small></td>");
                        out.println("<td width=\"150\" align='center'><form action=\"BookServlet\" method=\"GET\"><button type=\"submit\" value=\"" + re01.getString("ID") +"\" name=\"remove\">remove</button></form></td>");
                        out.println("</tr>");
                        /*out.println("<form action=\"BookServlet\" method=\"GET\">");
                        out.println("book Name:" + re01.getString("book_Name") + "        author:" + re01.getString("author") + "        ISBN:" + re01.getString("ISBN"));
                        out.println("<input type=\"submit\" value=\"RemoveID" + re01.getString("ID") + "\" name=\"remove\" />");
                        out.println("</form>");*/
                    }
                } catch (Exception e){
                    System.out.println("Cannot Show Search Result!"+ e);
                }
                out.println("</table>");
            }

            //close resultset;
            try{
                re01.close();
            } catch (Exception e){
                System.out.println("Cannot Close Resultset!"+ e);
            }

        } else if(Remove01 != null) {
            try{
                st01.executeUpdate("DELETE from book_list where ID =" + Remove01);
            } catch (Exception e){
                System.out.println("Cannot Delete Row!"+ e);
            }
        }

        //close html
        out.println("</body>");
        out.println("</html>");

        try{
            st01.close();
            co01.close();
        } catch (Exception e){
            System.out.println("Cannot Close Connection!"+ e);
        }
    }

    public void destroy()
    {
        // do nothing.

        //close connecttion;
    }


}
